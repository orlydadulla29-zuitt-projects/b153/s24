// MongoDB Query Operators and Field Projection

db.users.updateOne(
	{_id: ObjectId("61fbca5cc7a47272c0e0619d")},
	{
		$push: {
			skills: ["HTML", "CSS", "Javascript"]
			}
		}
)

db.users.insertMany(
[
	{
		name: "User1",
		age: 30,
		isActive: true,
		skills: ["HTML", "CSS", "Bootstrap"]
	},

	{
	name: "User2",
	age: 30,
	isActive: true,
	skills: ["HTML", "CSS", "Javascript"]
	},

	{
	name: "User3",
	age: 30,
	isActive: true,
	skills: ["HTML", "MERN"]
	},

	{
	name: "User4",
	age: 30,
	isActive: true,
	skills: ["HTML", "PHP", "MySQL"]
	},

	{
	name: "User5",
	age: 30,
	isActive: true,
	skills: ["Javascript", "Python"]
	},
])

//Delete a field

db.users.updateOne(
	{_id: ObjectId("61fbca5cc7a47272c0e0619d")},
	{
		$unset: {
			skills: ["HTML", "CSS", "Javascript"]
			}
		}
)


//search for users that know either Javascript or MERN

db.users.find({

	skills: {
		$in: ["Javascript", "MERN"]
	}
}
)

//search for users that know HTML and CSS

db.users.find({

	skills: {
		$all: ["HTML", "CSS"]
	}
}
)


//Show listings that are of property type "condominium", has a minimum stay of 1 night, has 1 bedroom, and can accommodate 2 people

//1 means true on  2nd object

db.listingsAndReviews.find(
	{
		property_type: "Condominium",
		minimum_nights: "2",
		bedrooms:1,
		accommodates: 2
	},

	{
		name: 1,
		property_type: 1,
		minimum_nights: 1,
		bedrooms: 1,
		accommodates: 1
	}
).limit(5) //how first 5 by _id


// Show listings that have a price of less than 100, fee for additional heads that is less than 20, and are in the country Portugal.


db.listingsAndReviews.find(
	{
		price: {
			$lt: 100
		},
		extra_people: {
			$lt: 20
		},
		"address.country": "Portugal"
	},
	{
		name: 1,
		price: 1,
		extra_people: 1,
		"address.country": 1
	}
).limit(5)

//search for the first 5 listings by name in ascending order that have a price less than 100.

db.listingsAndReviews.find(
	{
		price: {
			$lt: 100
		}
	},
	{
		name: 1,
		price: 1,
	}
).limit(5)
.sort({
	name: -1  // -1  For descending, 1 for ascending
})

//increase the price of the previous 5 listings by 10

db.listingsAndReviews.updateMany(
        {
	
		price: {
			$lt: 100
		}
	},
	{
		$inc: {
			price: -10 //use a negative number to decrease instead of increase
		}
	}
)


// delete listings with any one of the following conditions:
// review scores accuracy <=75
// review scores cleanliness <= 80
// review scores checkin <= 70
// review scores communication <= 70
// review scores location <= 75
// review scores value <= 80
db.listingsAndReviews.deleteMany(
  {
    $or:
    [ 
      {"review_scores.review_scores_accuracy": {$lte: 75}},
      {"review_scores.review_scores_cleanliness": {$lte: 80}},
      {"review_scores.review_scores_checkin": {$lte: 70}},
      {"review_scores.review_scores_communication": {$lte: 70}},
      {"review_scores.review_scores_location": {$lte: 75}},
      {"review_scores.review_scores_value": {$lte: 80}}
    ]
  }
)

//$gt and $gte can be used for greater than and greater than or equal to, respectively

